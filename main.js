window.onload = () =>{

                        ///Iteración #1: Interacción con el DOM

    //1.1 Usa querySelector para mostrar por consola el botón con la clase .showme

    /* const showme = document.querySelector('.showme');
    console.log(showme); */

    //1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado

    /* const pillado = document.querySelector('#pillado');
    console.log(pillado); */

    //1.3 Usa querySelector para mostrar por consola todos los p

    /* const p = document.querySelectorAll('p')
    console.log(p);*/
    //1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon

    /* const pokemon = document.querySelectorAll('.pokemon');
    console.log(pokemon); */

    //1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo
    //data-function="testMe".

    /* const testMe = document.querySelectorAll('[data-function="testMe"]');
    console.log(testMe); */

    //1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo
    //data-function="testMe".

    /* const testMe = document.querySelectorAll('[data-function="testMe"]');
    console.log(testMe[2]); */


                        ///Iteración #2: Modificando el DOM


    //2.1 Inserta dinamicamente en un html un div vacio con javascript.

        /*const createContent = () =>{
        const div = document.createElement('div');
        const parent = document.querySelector('body');
        parent.appendChild(div);
    }
    createContent(); */

    //2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

/*     const createContent = () =>{
        const div = document.createElement('div');
        const p = document.createElement('p');

        const parentDiv = document.querySelector('body');
        parentDiv.appendChild(div);
        div.appendChild(p);
    }
    createContent(); */

    //2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

/*     const createContent = () =>{
        const div = document.createElement('div');
        const parentDiv = document.querySelector('body');
        parentDiv.appendChild(div);
        for(i = 0; i < 6; i++){
            const p = document.createElement('p');
            div.appendChild(p)
        }
    }
    createContent(); */

    //2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

/*     const createContent = () =>{
        const p = document.createElement('p');
        p.textContent = 'Soy dinámico!';
        const parentP = document.querySelector('body');
        parentP.appendChild(p);
    }
    createContent()
     */

    //2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

/*     const createContent = () =>{
        let h2 = document.querySelector('.fn-insert-here');
        h2.innerHTML = 'hola';
    }
    createContent(); */

    //2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.

/*     const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

    const createContent = () =>{
        const ul = document.createElement('ul');
        const parentUl = document.querySelector('body');
        parentUl.appendChild(ul);
        apps.forEach(element => {
            let li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML = element;
        });
    }

        createContent(); */

    //2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

/*     const createContent = () =>{
        let remove = document.querySelectorAll('.fn-remove-me');
        remove.forEach(element =>{
            element.remove();
        })
    }
    createContent()
 */
    //2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div.
	//Recuerda que no solo puedes insertar elementos con .appendChild.

/*     const createContent = () =>{
        let p = document.createElement('p');
        p.innerHTML = 'Voy en medio!';
        parentP = document.querySelector('body');

        let div = document.querySelectorAll('div');
        //lista de divs
        console.log(div);
        //Inserta P antes del segundo DIV
        parentP.insertBefore(p,div[1]);
    }
    createContent();
    */

    //2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here


/*         let parent = document.querySelectorAll('div.fn-insert-here');
        console.log(parent);
        parent.forEach(element => {
            let p = document.createElement('p');
            p.innerHTML = 'Voy dentro!';
            element.appendChild(p);
        }); */
}



